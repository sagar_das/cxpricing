import React, { Component, Fragment, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { browserHistory } from 'react-router';
import logo from './cx_logo.png';
import './App.css';
import axios from 'axios';
import { List } from "react-virtualized";
import { environment } from './Environment'
import {graphql, QueryRenderer} from 'react-relay';
import Price from './Price';
import Pagination from './components/Pagination'
import PriceItem from  './components/PricePage'
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome,faAddressCard, faAddressBook, faAtlas, faCalculator, faChartPie } from '@fortawesome/free-solid-svg-icons'
import Accounts from './components/Accounts';
import Contacts from './components/Contacts';
import Pricing from './components/Pricing';
import Analytics from './components/Analytics';
import Calculation from './components/Calculation'
import ItemDetail from './components/ItemDetail';
import AccountDetail from './components/AccountDetail';
import ContactDetail from './components/ContactDetail';


var pageloading = false;
//const [pageloading, setpageLoading] = useState(false);
var currentPage = 1;
const itemsPerPage = 10;
export var currentItems;
var prices;

 // Get current items
 var indexOfLastItem = currentPage * itemsPerPage;
 var indexOfFirstItem = indexOfLastItem - itemsPerPage;


const API_URL = 'http://localhost:8080';


class App extends Component {


  constructor() {

    super();

  }


  render() {


    return (
      <Router>
      <Route render={({ location, history }) => (
      <React.Fragment>
        <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">CX Pricing platform</h1>
        </header>
        </div>
      <SideNav
              onSelect={(selected) => {
              // Add your code here
              var path = '/' + selected;
              console.log(selected);
              console.log(location.pathname);
              
                    if (location.pathname !== path) {
                        history.push(path);
                    }
                    return <Redirect to={path} />
              }}
      >
    <SideNav.Toggle />
      <SideNav.Nav defaultSelected="calculation">
        {/* <NavItem eventKey="pricing">
            <NavIcon>
            <FontAwesomeIcon icon={faHome} />
            </NavIcon>
            <NavText>
                Price
            </NavText>
        </NavItem>
        <NavItem eventKey="accounts">
            <NavIcon>
            <FontAwesomeIcon icon={faAtlas} />
            </NavIcon>
            <NavText>
                Account
            </NavText>
        </NavItem>
        <NavItem eventKey="contacts">
            <NavIcon>
            <FontAwesomeIcon icon={faAddressCard} />
            </NavIcon>
            <NavText>
                Contacts
            </NavText>
        </NavItem> */}
        <NavItem eventKey="calculation">
            <NavIcon>
            <FontAwesomeIcon icon={faCalculator} />
            </NavIcon>
            <NavText>
                Price Calculator
            </NavText>
        </NavItem>
        <NavItem eventKey="analytics">
            <NavIcon>
            <FontAwesomeIcon icon={faChartPie} />
            </NavIcon>
            <NavText>
                Analytics
            </NavText>
        </NavItem>
        </SideNav.Nav>
      </SideNav>
      <main>
                <Route path="/pricing" component={props => <Pricing />} />
                <Route path="/contacts" component={props => <Contacts />} />
                <Route path="/accounts" component={props => <Accounts />} />
                <Route path="/analytics" component={props => <Analytics />} />
             
                {/* <Route path="/itemdetail" component={props => <ItemDetail />} /> */}
            <Route path="/calculation" render={(props) => <Calculation {...props} />} />
            <Route path="/itemdetail" render={(props) => <ItemDetail {...props} />} />
            <Route path="/accountdetail" render={(props) => <AccountDetail {...props} />} />
            <Route path="/contactdetail" render={(props) => <ContactDetail {...props} />} />
            </main>
      
      
      </React.Fragment>
      )}
      />
      </Router>
    );

   
          
  }
}

export default App;
