import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter,Switch,Route } from 'react-router-dom'
import {Router, browserHistory} from 'react-router';
import Accounts from './components/Accounts';
import Contacts from './components/Contacts';
import Pricing from './components/Pricing';
import Analytics from './components/Analytics';
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'


// 2
const httpLink = createHttpLink({
  uri: 'http://localhost:8080/graphql'
})

// 3
export const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
})

ReactDOM.render((<BrowserRouter>
    <ApolloProvider client={client}>
    <App />
    </ApolloProvider>
  </BrowserRouter>), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={App} />
      <Route path="/accounts" component={Accounts} />
      <Route path="/contacts" component={Contacts} />
      <Route path="/pricing" component={Pricing} />
      <Route path="/analytics" component={Analytics} />
    </Switch>
  );
}
