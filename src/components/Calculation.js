import React, { Component, Fragment, useState, useCallback } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect, withRouter } from 'react-router-dom';
import '../App.css';
import {graphql, QueryRenderer} from 'react-relay';
import gql from 'graphql-tag'
import { Query, withApollo } from 'react-apollo'
import {useDropzone} from 'react-dropzone'
import Dropzone from 'react-dropzone'
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import {client} from '../index'
import CalculationItem from './CalculationItem'

var pageloading = false;
//const [pageloading, setpageLoading] = useState(false);
var currentPage = 1;
const itemsPerPage = 10;
export var currentItems;
var contacts;

 // Get current items
 var indexOfLastItem = currentPage * itemsPerPage;
 var indexOfFirstItem = indexOfLastItem - itemsPerPage;


const API_URL = 'http://localhost:8080';

const gql_query = gql`
query priceById($id: ID){
  
  priceById(id : $id ){
                id
                regPrice
                discountPrice
                product_id
              }


}
`




class Calculation extends Component {


  state = {
    viewResults : false
  }

  constructor(){
    super()
    this.parseExcelFile = this.parseExcelFile.bind(this)
    this.showData = this.showData.bind(this)
    this.state = {
      viewResults : false
    }
    this.resultDiv = null
  }

  parseExcelFile(acceptedFiles){
    console.log(acceptedFiles)
    var fileObj = acceptedFiles[0]
    ExcelRenderer(fileObj, (err, resp) => {
      if(err){
        console.log(err);            
      }
      else{
        this.setState({
          cols: resp.cols,
          rows: resp.rows
        });
        console.log("File parsed successfully")
      }
    });  
  }

  calculateDiscountPrice(regularPrice){
    return (regularPrice - 5)
  }
  

  async showData(){
    console.log(this.state.cols)
    console.log(this.state.rows)
    var rows = this.state.rows
    var items =[]
    for (var i = 1; i < rows.length; i++){
      var { data } = await client.query({
        query: gql_query,
        variables: { id: rows[i][1] },
      })

      var discountPrice = this.calculateDiscountPrice(data.priceById.regPrice)

      items.push({id: data.priceById.id, regPrice: data.priceById.regPrice, discountPrice: discountPrice})
  
      console.log(data)
    }

    this.resultDiv = <CalculationItem items={items} />
    this.setState({viewResults: true})

  }
  
  
  render(){

    console.log("Rendered")
  
      return(
        <React.Fragment>
          <div className="div-parent">
        <Dropzone onDrop={acceptedFiles => 
          this.parseExcelFile(acceptedFiles)
          
          }>
  {({getRootProps, getInputProps}) => (
    <section>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <p>Drag 'n' drop some files here, or click to select files</p>
      </div>
    </section>
  )}
</Dropzone>
</div>
      <div className="div-parent">
      <button onClick={this.showData}>Display data</button>
      <div className='form-container'>
            {(this.state.viewResults) ? this.resultDiv : ''}
      </div>
      </div>
</React.Fragment>
      );
  }
}

withApollo(Calculation);
export default withRouter(Calculation);