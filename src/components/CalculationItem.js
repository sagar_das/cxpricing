import React from 'react';
import '../App.css';
import logo from '../contacts.png';

var CalculationItem = ({ items }) => {
  
  console.log("Calculation item")
  console.log(items)
  return (
    <div className="div-parent">
              {items.map((item, i) => 
              <div className="card">
                       <div className="container">
                         <h4><b>ID: {item.id}</b></h4>
                         <p>Regular price: {item.regPrice}</p>
                         <p>Discount price: {item.discountPrice}</p>
                       </div>
                       </div>
              )}
            </div>
  );
};

export default CalculationItem;